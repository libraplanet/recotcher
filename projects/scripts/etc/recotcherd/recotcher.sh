#!/bin/sh


#------------------
# general settings
#------------------
LOCK_FILE_NAME=.lock
CNT_FILE_NAME=.cnt
RTMP_URL="rtmp://localhost/radio"
FORMAT_MP4_EXT=.mp4
FORMAT_MP4_PARAMS="-vcodec copy -acodec copy -f mp4"
FORMAT_M4A_EXT=.m4a
FORMAT_M4A_PARAMS="-vn -acodec copy -f mp4"
FORMAT_AAC_EXT=.aac
FORMAT_AAC_PARAMS="-vn -acodec copy -f adts"
FORMAT_MKV_EXT=.mkv
FORMAT_MKV_PARAMS="-vcodec copy -acodec copy -f matroska"


function lock() {
  local cnt=0
  local max=20
  echo "lock start."
  until `ln -s $$ "$1"`
  do
    cnt=$((cnt + 1))
    if [ ${cnt} -gt ${max} ]; then
      return 1
    fi

    echo "[${cnt}/${max}] wait."
    sleep 5
  done
  trap unlock EXIT
  echo "locking."
  return 0
}


function unlock () {
  if [ -L "$1" ]; then
    rm -f "$1"
  fi
  echo "unlocked."
  trap "" EXIT
}


function recording () {
  local ff_input="$1"
  local ff_timeout_sec="$2"
  local ff_params="$3"
  local ff_output="$4"
  local killsec=$((${ff_timeout_sec} + 60))

  echo start ffmpeg.
  timeout -sKILL ${killsec} ffmpeg -t ${ff_timeout_sec} -y -re -i "${ff_input}" ${ff_params} "${ff_output}"
}

function loadConf() {
  local base_dirpath=`dirname ${0}`
  local conf_dirname="${1}.conf"
  local str_date=`date --d "10 min"  +"%w%H%M"`
  local sWeek=${str_date:0:1}
  local sHour=${str_date:1:2}
  local sTime=${str_date:3:2}
  local nTime=$((10#${sTime} / 30 * 30))
  local fname=`printf "%s_%s%02d%s" "${sWeek}" "${sHour}" "${nTime}" ".conf"`
  echo conf file=${conf_dirname}/${fname}
  . "${base_dirpath}/${conf_dirname}/${fname}"
}

function waitFirst () {
  local date0=`date +"%y%m%d %H:%M:00"`
  local date1=`date --d "${date0} 1 minutes" +"%y%m%d %H:%M:%S"`
  local date2=`date --d "${date1} 0 sec ago" +"%y%m%d %H:%M:%S"`
  local sec0=`date +"%s"`
  local sec1=`date --d "${date2}" +"%s"`
  local dist=$((${sec1} - ${sec0}))
  #echo "date0 = ${date0}"
  #echo "date1 = ${date1}"
  #echo "date2 = ${date2}"
  #echo "sec0  = ${sec0}"
  #echo "sec1  = ${sec1}"
  #echo "dist  = ${dist}"
  #echo `date +"%y/%m/%d %H:%M:%S"`
  if [ $dist -gt 0 ]; then
    echo "sleep (${dist})"
    sleep "${dist}"
  else
    echo "no wait. (${dist})"
  fi
}

function main () {
  local cnt=0
  local output_cnt=
  local output_format="[%s] No.%03d_%s"
  local output_date=`date --d "10 min" +"%y%m%d"`
  local output_fname=

  echo load conf.
  loadConf "${1}"
  echo BASE_DIR=${BASE_DIR}
  echo REC_SEC=${REC_SEC}
  echo REC_TYPE=${REC_TYPE}
  echo CHANNEL_TITLE=${CHANNEL_TITLE}

  # init base directory
  mkdir -p "${BASE_DIR}"

  # cd to base directory
  if ! cd "${BASE_DIR}"; then
    echo failed cd to base directory. 1>&2
    exit 1
  fi
  echo pwd="${PWD}"

  waitFirst

  echo start recording.

  # increment onair no.
  if ! lock "${LOCK_FILE_NAME}"; then
    echo failed lock. 1>&2
    exit 1
  fi

  if [ -f "${CNT_FILE_NAME}" ]; then
    cnt=`cat "${CNT_FILE_NAME}"`
  fi
  output_cnt=$((cnt + 1))
  echo "${output_cnt}" > "${CNT_FILE_NAME}"

  unlock "${LOCK_FILE_NAME}"

  # create base file name.
  output_fname=`printf "${output_format}" "${output_date}" "${output_cnt}" "${CHANNEL_TITLE}"`

  # trace.
  echo output_cnt=${output_cnt}
  echo output_fname=${output_fname}

  # execute recording.
  case ${REC_TYPE} in
    mp4) echo "case ${REC_TYPE}"; recording "${RTMP_URL}" "${REC_SEC}" "${FORMAT_MP4_PARAMS}" "${output_fname}${FORMAT_MP4_EXT}";;
    m4a) echo "case ${REC_TYPE}"; recording "${RTMP_URL}" "${REC_SEC}" "${FORMAT_M4A_PARAMS}" "${output_fname}${FORMAT_M4A_EXT}";;
    aac) echo "case ${REC_TYPE}"; recording "${RTMP_URL}" "${REC_SEC}" "${FORMAT_AAC_PARAMS}" "${output_fname}${FORMAT_AAC_EXT}";;
    mkv) echo "case ${REC_TYPE}"; recording "${RTMP_URL}" "${REC_SEC}" "${FORMAT_MKV_PARAMS}" "${output_fname}${FORMAT_MKV_EXT}";;
    *)   echo "case ${REC_TYPE}"; ;;
  esac

  echo stop recoding.
}

main $@
