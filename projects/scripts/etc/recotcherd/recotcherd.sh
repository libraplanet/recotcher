#!/bin/sh

function waitPolling () {
  sSec=`date +"%S"`
  iSec=$(( 60 - 10#${sSec} ))
  sleep ${iSec}
}

function checkTime () {
  local d=`date +"%y%m%d%H%M%S"`
  local year=${d:0:2}
  local month=${d:2:2}
  local day=${d:4:2}
  local hour=${d:6:2}
  local min=${d:8:2}
  local sec=${d:10:2}
  #echo `printf "%s/%s/%s %s:%s:%s" "${year}" "${month}" "${day}" "${hour}" "${min}" "${sec}"`

  case "${year}" in
    *)
       case "${month}" in
         *)
           case "${day}" in
             *)
               case "${hour}" in
                 *)
                   case "${min}" in
                     #* )
                     "29" | "59" )
                       #case "${sec}" in
                       #    "45") return 0
                       #    ;;
                       #esac
                       return 0
                     ;;
                   esac
                 ;;
               esac
             ;;
           esac
         ;;
       esac
    ;;
  esac

  return 1
}

function main() {
  local scriptFname="recotcher.sh"
  local basedir=`dirname ${0}`
  local scriptPath="${basedir}/${scriptFname}"
  echo "script path = ${scriptPath}"
  echo "script args = $@"
  while :
  do
    echo `date +"%y/%m/%d %H:%M:%S"`

    if checkTime ; then
      echo "sh \"${scriptPath}\" $@ &"
      sh "${scriptPath}" $@ &
    fi 

    waitPolling
  done
}

main $@
