package org.libraplanet.recotcher;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.util.CellReference;

public class Main {
	private static void closeQuietly(final Closeable o) {
		try{
			o.close();
		} catch(Exception e){
		}
	}

	/**
	 * http://www.ne.jp/asahi/hishidama/home/tech/apache/poi/cellreference.html
	 * @params sheet Sheet
	 * @params pos position
	 * @return 
	 */
	private static Cell getCell(final Sheet sheet, final String pos) {
		final CellReference reference = new CellReference(pos);
		final Row row = sheet.getRow(reference.getRow());
		if(row == null) {
			return null;
		} else {
			return row.getCell(reference.getCol());
		}
	}

	private static String getFixedStringValue(Cell cell) {
		//System.out.println(cell);
		switch(cell.getCellType()) {
			case Cell.CELL_TYPE_FORMULA:
				{
					Workbook book = cell.getSheet().getWorkbook();
					CreationHelper helper = book.getCreationHelper();
					FormulaEvaluator evaluator = helper.createFormulaEvaluator();
					return getFixedStringValue(evaluator.evaluateInCell(cell));
				}
			case Cell.CELL_TYPE_STRING:
				return cell.getStringCellValue();
			case Cell.CELL_TYPE_NUMERIC:
				return Double.toString(cell.getNumericCellValue());
			case Cell.CELL_TYPE_BOOLEAN:
				return Boolean.toString(cell.getBooleanCellValue());
			case Cell.CELL_TYPE_ERROR:
				return Integer.toString(cell.getErrorCellValue());
			case Cell.CELL_TYPE_BLANK:
			return "";
		}
		return null;
	}

	public static void main(String[] args) {
		final String xlsFname;
		FileInputStream fiStream = null;
		Workbook workbook = null;

		System.out.println("#-----------------------------------------------------------------------");
		System.out.println("# Recotcher");
		System.out.println("#   auther : takumi");
		System.out.println("#-----------------------------------------------------------------------");
		System.out.println("hellow world.");
		System.out.println(MessageFormat.format("{0} = {1}", new Object[]{"args[0]", args[0]}));

		//引数
		{
			xlsFname = args[0];
		}
		//
		{
			try {
				fiStream = new FileInputStream(xlsFname);
				workbook = new XSSFWorkbook(fiStream);
				final String outDir;
				final String programSheetName;
				final String programStartRowNo;
				final String varNameDir;
				final String varRecSec;
				final String varRecType;
				final String varChannelTitle;
				final String colShName;
				final String colNameDir;
				final String colRecSec;
				final String colRecType;
				final String colChannelTitle;
				//setting
				{
					System.out.println("");
					System.out.println("");
					System.out.println("[read settings sheet]");
					Sheet sheet = workbook.getSheet("settings");
					//出力先フォルダ
					outDir = getFixedStringValue(getCell(sheet, "C11"));
					//番組用シート名
					programSheetName = getFixedStringValue(getCell(sheet, "C12"));
					//開始行番号
					programStartRowNo = getFixedStringValue(getCell(sheet, "C13"));

					// 変数名 - フォルダ名
					varNameDir = getFixedStringValue(getCell(sheet, "C18"));
					// 変数名 - 録音時間
					varRecSec = getFixedStringValue(getCell(sheet, "C19"));
					// 変数名 - 録音タイプ
					varRecType = getFixedStringValue(getCell(sheet, "C20"));
					// 変数名 - 番組タイトル
					varChannelTitle = getFixedStringValue(getCell(sheet, "C21"));

					// カラム - スクリプト名 
					colShName = getFixedStringValue(getCell(sheet, "D17"));
					// カラム - フォルダ名
					colNameDir = getFixedStringValue(getCell(sheet, "D18"));
					// カラム - 録音時間
					colRecSec = getFixedStringValue(getCell(sheet, "D19"));
					// カラム - 録音タイプ
					colRecType = getFixedStringValue(getCell(sheet, "D20"));
					// カラム - 番組タイトル
					colChannelTitle = getFixedStringValue(getCell(sheet, "D21"));
				}

				//programs
				{
					System.out.println("");
					System.out.println("");
					System.out.println("[read programs sheet]");
					System.out.println(MessageFormat.format("{0} = {1}", new Object[]{"program sheet name", programSheetName}));
					final Sheet sheet = workbook.getSheet(programSheetName);
					// カラム - スクリプト名
					final int colIndexShName = CellReference.convertColStringToIndex(colShName);
					// カラム - フォルダ名
					final int colIndexNameDir = CellReference.convertColStringToIndex(colNameDir);
					// カラム - 録音時間
					final int colIndexRecSec = CellReference.convertColStringToIndex(colRecSec);
					// カラム - 録音タイプ
					final int colIndexRecType = CellReference.convertColStringToIndex(colRecType);
					// カラム - 番組タイトル
					final int colIndexChannelTitle = CellReference.convertColStringToIndex(colChannelTitle);

					int startRowNo = (int)Double.parseDouble(programStartRowNo);
					int rowIndexStart = startRowNo - 1;
					int rowIndexLast = sheet.getLastRowNum();
					System.out.println(MessageFormat.format("{0} = {1}", new Object[]{"start row no   ", startRowNo}));
					System.out.println(MessageFormat.format("{0} = {1}", new Object[]{"start row start index", rowIndexStart}));
					System.out.println(MessageFormat.format("{0} = {1}", new Object[]{"start row last index", rowIndexLast}));
					for(int i = rowIndexStart; i <= rowIndexLast; i++){
						final Row row = sheet.getRow(i);
						System.out.println("");
						System.out.println(MessageFormat.format("  [{0}/{1}]", new Object[]{i, rowIndexLast}));
						if(row.getCell(0) == null) {
							System.out.println(MessageFormat.format("    {0}", new Object[]{null}));
						} else if(row.getRowNum() == 0) {
							System.out.println(MessageFormat.format("    {0}", new Object[]{"blank row."}));
						} else {
							String confPath = getFixedStringValue(row.getCell(colIndexShName));
							if(StringUtils.isBlank(confPath)) {
								System.out.println(MessageFormat.format("    {0}", new Object[]{"blank output."}));
							} else{
								FileOutputStream foStream = null;
								OutputStreamWriter osWriter = null;
								final String path = FilenameUtils.concat(outDir, confPath);
								final String[][] lines = new String[][]{
									new String[]{varNameDir,      getFixedStringValue(row.getCell(colIndexNameDir))},
									new String[]{varRecSec,       getFixedStringValue(row.getCell(colIndexRecSec))},
									new String[]{varRecType,      getFixedStringValue(row.getCell(colIndexRecType))},
									new String[]{varChannelTitle, getFixedStringValue(row.getCell(colIndexChannelTitle))},
								};
								System.out.println(MessageFormat.format("    {0}", new Object[]{path}));
								try {
									File f = new File(path);
									foStream = FileUtils.openOutputStream(f);
									osWriter = new OutputStreamWriter(foStream, "UTF-8");
									for(String[] strs : lines) {
										final String line = MessageFormat.format("{0}={1}", (Object[])strs);
										System.out.println(MessageFormat.format("      {0}", new Object[]{line}));
										osWriter.write(line + "\n");
									}
								} finally{
									closeQuietly(osWriter);
									closeQuietly(foStream);
								}
							}
						}
					}
				}
			} catch(Exception e){
				e.printStackTrace(System.out);
				System.exit(1);
			} finally{
				closeQuietly(workbook);
				closeQuietly(fiStream);
			}
			System.exit(0);
		}
	}
}
